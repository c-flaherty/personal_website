module.exports = {
  assetsDir: 'assets',
	baseUrl: '/',
  pwa: {
    name: 'Colin Flaherty',
    themeColor: '#EB7961',
    msTileColor: '#EB7961'
  },
  pluginOptions: {
    quasar: {
      theme: 'ios',
			extras: [
				'fontawesome',
			],
			framework: {
  			iconSet: 'fontawesome',
			},
    }
  },

  transpileDependencies: [
    /[\\\/]node_modules[\\\/]quasar-framework[\\\/]/
  ]
}
