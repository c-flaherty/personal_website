declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
  }

declare module 'quasar-framework'
declare module 'quasar'

/*
declare module 'quasar' {
	import QuasarF from 'quasar-framework/src/index.esm.js'
	export default QuasarF
}


declare module 'quasar-framework/*' {
	import QuasarF from 'quasar-framework/dist/quasar.ios.esm.js'
	export default QuasarF
	}
*/
