# Colin Michael Flaherty's Personal Website

What better way to showcase my web development skills than to build a great personal website! 


[![pipeline status](https://gitlab.com/c-flaherty/personal_website/badges/master/pipeline.svg)](https://gitlab.com/c-flaherty/personal_website/commits/master)     
     
(Mirrored via GitLab at https://gitlab.com/c-flaherty/personal_website.)

# Who Am I
I am a rising second year at the University of Chicago. Currently, I am planning on majoring in computer science and mathematics with a specialization in economics. 

If I'm not working on schoolwork, I'm probably coding, participating [in a student-run club](https://www.uchicagotechteam.com/), or [volunteering](https://www.gatewayoutdoors.org/).  
If I'm not coding, attending a club, or volunteering, I'm probably at the gym.  
If I'm not at the gym, I may be reading a good book that just might change my life such as [this](https://www.goodreads.com/book/show/240976.The_Book_of_Laughter_and_Forgetting), [this](https://www.goodreads.com/book/show/174518.The_Aesthetics_of_Resistance_Vol_1), or [this](https://www.goodreads.com/book/show/333538.The_Castle?ac=1&from_search=true).     
If I'm lucky to find time for anything else, I'm probably making music whether that be DJ'ing, playing piano and guitar, or digitally producing music.   

## Purpose
While establishing a better online presence is one of the main goals of building my own personal website, I am primarily building my own website to have a place to implement all the new tools, techniques, and features that I learn in my journey into web development, design, and computer science. Hence, this website will always be a work-in-progress that hopefully grows as I grow. I am excited to implement some of my favorite tools such as Vue, Babel, ESLint, Cypress, and more, while also gaining practice in using or implementing newly-learned tools such as Storybook UI or the recently-released Vue-CLI 3.  

Here are some really well-designed personal websites that I am taking inspiration from:
  - https://caferati.me/
  - http://findmatthew.com/
  - http://ejosue.com/
  - http://www.narrowdesign.com/
  - http://www.pascalvangemert.nl/
  - http://www.adamhartwig.co.uk/
  - http://andrevv.com/
  - http://www.garylemasson.com/
  - http://www.anthonydesigner.com/
  - http://tanmade.com/
  - http://ianenders.com/ 
  - http://seanhalpin.io/
  - http://tonydorio.com/
  - http://www.adhamdannaway.com/
  
This website repository is public, so that other budding developers [can build off it](https://www.goodreads.com/quotes/531831-immature-poets-imitate-mature-poets-steal-bad-poets-deface-what).

## Methodology
I am a big fan of the [Twelve Factor App](https://12factor.net/) set of web development design patterns, so I'm going to follow these for my personal website as best I can.

### Git Branching Model
To keep this repository organized, I will use the same git branching model I would use for any professional project, as outlined Vincent Driessen's very popular ["A Successful Git Branching Model"](https://nvie.com/posts/a-successful-git-branching-model/), but with one small alteration: For convenience, production-ready versions are found on a "production" branch (as opposed to the "master" branch) and in-development versions are found on the "master" branch (as opposed to a "develop" branch). This change was inspired by [this critique of Driessen's branching model](https://barro.github.io/2016/02/a-succesful-git-branching-model-considered-harmful/).

### Versioning
For versioning, I use the popular ["Semantic Versioning System"](https://semver.org/).

### CI/CD
While I keep a copy of the repository here on Github so that fellow developers such as yourself can view it, I also store a [parallel](https://moox.io/blog/keep-in-sync-git-repos-on-github-gitlab-bitbucket/) copy on Gitlab, so that I may use Gitlab's great built-in CI/CD tools. This will involve hosting a Gitlab Runner on an Amazon Web Services' Elastic Compute Cloud or Google Cloud's Compute Engine.

### Environment Variables
Sensitive variables are stored as environment variables, whether that be in a [`.env`](https://www.npmjs.com/package/dotenv) or via Gitlab's environment variable management system for CI/CD. Hence, this project will not be buildable out-of-the-box until you repopulate these environment variables. To ease this process, a `.env.example` file is included to list what environment variables must be set.

### Deployment
At first, the site will be statically generated and then served via an Amazon Web Services' S3 bucket because this is cheapest. It will also be pushed to Amazon Web Services' CloudFront content delivery network, so that it can get to viewers like you faster. Should the need arise (this section will be updated if and/or when this need does arise), I will serve it from an Amazon Web Services' Elastic Compute Cloud or Google Cloud's Compute Engine via a [dockerized Express instance hidden behind NGINX](https://auth0.com/blog/load-balancing-nodejs-applications-with-nginx-and-docker/).
